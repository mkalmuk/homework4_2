package homework4.task2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

//import ColorDTO;

public class App {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		FileInputStream file = new FileInputStream("src/main/resources/colors.json");
		//System.out.println(file.getAbsolutePath());
		
		ObjectMapper objectMapper = new ObjectMapper();
		//objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
		TypeFactory typeFactory = TypeFactory.defaultInstance();
		List<ColorDTO> colors = objectMapper.readValue(file, typeFactory.constructCollectionType( 
						ArrayList.class, ColorDTO.class));
		
		// print parsed object 
		System.out.println("Colors q-ty: " + colors.size()); // 6
		System.out.println(colors.get(0).toString());
		System.out.println(colors.get(1).toString());
		System.out.println(colors.get(2).toString());
		System.out.println(colors.get(3).toString());
		System.out.println(colors.get(4).toString());
		System.out.println(colors.get(5).toString());
	}
}
