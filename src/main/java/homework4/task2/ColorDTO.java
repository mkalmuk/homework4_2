package homework4.task2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ColorDTO {

	@JsonProperty("color")
	private String color;
	
	@JsonProperty("category")
	private String category;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("hex")
	private String hex;
   
	@Override
	public String toString() {
		return "Color [color = " + color + ", category = " + category + ", type = " + type + ", hex = " + hex + "]";
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}

}
